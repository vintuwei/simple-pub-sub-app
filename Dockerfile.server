FROM gradle:6.8.3-jdk11 as clean_build
ARG GRADLE_ARGS
ARG GRADLE_TARGETS="clean --build-cache shadowJar"

COPY --chown=1000:1000 server/build.gradle ./server/
COPY --chown=1000:1000 server/settings.gradle ./server/
COPY --chown=1000:1000 server/src/ ./server/src/
RUN gradle ${GRADLE_TARGETS}

##################################################################################
#                                                                                #
#            Stage 2: Copy fat-jar and configuration YML to JRE image            #
#                                                                                #
##################################################################################

FROM openjdk:11-jre-slim
ENV APP_NAME=pubsub-server
ENV JAR_FILE=${APP_NAME}.jar

# Declare container user variables
ARG CONTAINER_USER_NAME=pubsub-server
ARG CONTAINER_USER_ID=545676

# Create application user
RUN id -u $CONTAINER_USER_NAME 2>/dev/null || useradd --system --create-home \
    --uid $CONTAINER_USER_ID --gid 0 $CONTAINER_USER_NAME
EXPOSE 8080

# Copy artifacts from gradle builder image and the entrypoint script
WORKDIR /opt/${APP_NAME}
RUN chown -R ${CONTAINER_USER_NAME}:root /opt/${APP_NAME}
COPY --from=clean_build /home/gradle/build/libs/${JAR_FILE} ./

# Set application user as default user
USER $CONTAINER_USER_NAME
ENTRYPOINT java -jar ${JAR_FILE}
