# Simple Pub Sub Application

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

Install Docker and Docker Compose
Install Java 11

PS: Because of time i wasn't able to fully setup the project on docker

### Running The Application

Start redis by running the following command

```
docker-compose up
```

Start the Server

```
cd server
gradle bootRun
```

Start the Client
```
cd client
gradle bootRun
```

## Tests

Run Client Tests:

```
cd client
gradle test
```

NB: Wasn't able to write server tests because of limited time.

## Author

* **Vincent Tuwei** - https://github.com/vtuwei, https://github.com/vintuwei