package com.pangaea.pubsub.server.representation.request;

import java.util.Map;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class Event {

    @NotNull
    String topic;

    @NotEmpty
    Map<String, Object> data;
}
