package com.pangaea.pubsub.server.configuration.executor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ThreadPoolConfig {
    Integer corePoolSize;
    String poolNameFormat;
    Integer maximumPoolSize;
    Integer keepAliveTimeInMilliSeconds;
    Boolean allowCoreThreadTimeOut;
}
