package com.pangaea.pubsub.server.util;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RedisLockUtil {

    private static String REDIS_LOCK_KEY = "REDIS_LOCK_KEY_";

    /**
     * Lock timeout to prevent threads from waiting indefinitely after entering the lock
     */
    private int EXPIRE_TIME_MSECS = 60 * 1000;

    @Autowired
    RedisTemplate redisTemplate;

    public void lock(String suffix) throws InterruptedException {
        tryLock(suffix);
    }

    public boolean tryLock(String suffix) throws InterruptedException {
        return tryLock(suffix, EXPIRE_TIME_MSECS, TimeUnit.MILLISECONDS);
    }

    private boolean tryLock(String suffix, long l, TimeUnit timeUnit) throws InterruptedException {
        String key = REDIS_LOCK_KEY + suffix;
        try {
            if (redisTemplate.hasKey(key)) {
                Boolean setNx = redisTemplate.opsForValue()
                    .setIfAbsent(key, UUID.randomUUID().toString(), l, timeUnit);
                if (setNx) {
                    return Boolean.TRUE;
                }
            }
        } catch (Exception e) {
            log.error("redis tryLock Exception", e);
            throw new InterruptedException(e.getMessage());
        }
        return Boolean.FALSE;
    }

    public void unlock(String suffix) {
        String key = REDIS_LOCK_KEY + suffix;
        redisTemplate.expire(key, 0, TimeUnit.MICROSECONDS);
    }
}
