package com.pangaea.pubsub.server.service;

import com.pangaea.pubsub.server.api.service.SubscriberApiService;
import com.pangaea.pubsub.server.redis.model.TopicSubscriber;
import com.pangaea.pubsub.server.redis.repository.TopicSubscriberRepository;
import com.pangaea.pubsub.server.representation.request.Event;
import com.pangaea.pubsub.server.representation.response.PublishEventResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class PublishingService {

    // used for concurrent publishing of events
    @Qualifier("publishingExecutorService")
    private final ExecutorService publishingExecutorService;

    protected final SubscriberApiService subscriberApiService;
    protected final TopicSubscriberRepository repository;

    public void publish(String topic, Map<String, Object> body) {

        // retrieves all urls that are subscribed to a given topic.
        Optional<TopicSubscriber> topicSubscriberOptional = repository.findById(topic);

        if (topicSubscriberOptional.isPresent()) {

            List<String> urls = topicSubscriberOptional.get().getUrls();

            if (urls.size() > 0) {
                // send messages to urls.
                try {
                    publishEvents( urls, Event
                        .builder()
                        .data(body)
                        .topic(topic)
                        .build());
                } catch (InterruptedException e) {
                    log.error("Exception encountered. ", e);
                }
            }
        } else {
            log.info("Topic {} is not found. Ignoring.", topic);
        }
    }

    /**
     * publishes events to upstream servers concurrently.
     * @param urls
     * @param event
     * @throws InterruptedException
     */
    private void publishEvents(List<String> urls, Event event) throws InterruptedException {
        List<Callable<Optional<PublishEventResponse>>> tasks = new ArrayList<>();

        for (String url : urls) {
            Callable<Optional<PublishEventResponse>> c = publishEvent(event, url);
            tasks.add(c);
        }

        // execute the requests in parallel
        List<Future<Optional<PublishEventResponse>>> results = publishingExecutorService.invokeAll(tasks);

        for (Future<Optional<PublishEventResponse>> r : results) {
            try {
                r.get();
            } catch (InterruptedException | ExecutionException e) {

                log.error("Exception encountered while publishing events to upstream servers. event={}", event, e);
                // custom exception might need to be thrown depending on use-case
            }
        }
    }

    public Callable<Optional<PublishEventResponse>> publishEvent(Event event, String url) {
        return () -> {
            try {
                log.info("publishing event to upstream server. url={}, event={}", url, event);
                return subscriberApiService.publishToUpstreamServer(url, event);

            } catch (Exception e) {
                log.error("Exception encountered while publishing event to upstream server. url={}, event={}",
                    url, event, e);
                // custom exception might need to be thrown depending on use-case
            }
            return Optional.empty();
        };
    }
}
