package com.pangaea.pubsub.server.api.service;

import com.pangaea.pubsub.server.api.client.SubscriberApiClient;
import com.pangaea.pubsub.server.representation.request.Event;
import com.pangaea.pubsub.server.representation.response.PublishEventResponse;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import retrofit2.Response;

@Service
@Slf4j
@RequiredArgsConstructor
public class SubscriberApiService {

    private final SubscriberApiClient subscriberApiClient;

    public Optional<PublishEventResponse> publishToUpstreamServer(String url, Event event) throws Exception {

        Response<PublishEventResponse> response = subscriberApiClient.publishToUpstreamServer(url, event).execute();

        HttpStatus httpStatus = HttpStatus.valueOf(response.code());

        try {
            if (httpStatus.is2xxSuccessful()) {
                log.info("event successfully published to upstream server. url={}, event={}, response={}",
                    url, event, response.body());

                return Optional.of(response.body());
            } else {
                String error = String.format("failed to publish event to upstream server. " +
                        "url=%s, event=%s, errorCode=%d, response=%s", url, event.toString(), response.code(),
                    response.errorBody().toString());
                log.error(error);
                throw new Exception(error); // TODO create a custom exception class
            }
        } catch (Exception e) {
            log.info("failed to publish event to upstream server. url={}, event={}", url, event, e);
            throw e;
        }
    }
}
