package com.pangaea.pubsub.server.api.client;

import com.pangaea.pubsub.server.representation.request.Event;
import com.pangaea.pubsub.server.representation.response.PublishEventResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface SubscriberApiClient {

    @POST
    Call<PublishEventResponse> publishToUpstreamServer(@Url String url, @Body Event event);
}
