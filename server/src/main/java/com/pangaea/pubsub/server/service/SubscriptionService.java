package com.pangaea.pubsub.server.service;

import com.pangaea.pubsub.server.redis.model.TopicSubscriber;
import com.pangaea.pubsub.server.redis.repository.TopicSubscriberRepository;
import com.pangaea.pubsub.server.util.RedisLockUtil;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class SubscriptionService {

    private final TopicSubscriberRepository topicSubscriberRepository;
    private final RedisLockUtil redisLockUtil;

    /**
     * Simple implementation that uses redis to record the URLs that are listening to the specified topic.
     * This solution is not scalable as URLs are being added to a set.
     * @param topic
     * @param url
     * @throws InterruptedException
     */
    public void subscribeToTopic(String topic, String url) throws InterruptedException {

        // check if topic exists
        Optional<TopicSubscriber> topicSubscriberOptional = topicSubscriberRepository.findById(topic);
        redisLockUtil.lock(topic);

        if (topicSubscriberOptional.isPresent()) {
            TopicSubscriber topicSubscriber = topicSubscriberOptional.get();

            // check if url already exists in cache
            if (topicSubscriber.getUrls().contains(url)) {
                return;
            }
            topicSubscriber.getUrls().add(url);
            topicSubscriberRepository.save(topicSubscriber);
        } else {
            topicSubscriberRepository.save(
                TopicSubscriber.builder()
                    .topic(topic)
                    .urls(List.of(url))
                    .build());
        }

        redisLockUtil.unlock(topic);
    }
}
