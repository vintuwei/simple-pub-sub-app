package com.pangaea.pubsub.server.configuration;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.pangaea.pubsub.server.configuration.executor.ThreadPoolConfig;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class ExecutorServiceConfiguration {

    public static final String PUBLISHING_EXECUTOR_NAME = "publishing-executor";

    @Qualifier("publishingConfig")
    private final ThreadPoolConfig migrationsConfig;

    @Bean("publishingExecutorService")
    public ExecutorService providesPublishingExecutorService() {
        return newThreadExecutorPool(migrationsConfig, PUBLISHING_EXECUTOR_NAME, new LinkedBlockingQueue<>());
    }

    private ExecutorService newThreadExecutorPool(ThreadPoolConfig threadPoolConfig,
                                                  String executorName, BlockingQueue<Runnable> workQueue) {
        int noOfCores = Runtime.getRuntime().availableProcessors();
        log.info("Parallelism factor based on no of processors for {} is {}", executorName, noOfCores);
        ThreadFactory threadFactory = new ThreadFactoryBuilder()
            .setNameFormat(threadPoolConfig.getPoolNameFormat()).build();
        ThreadPoolExecutor fixedThreadPoolExecutor;
        int corePoolSize = threadPoolConfig.getCorePoolSize() * noOfCores;
        int maximumPoolSize = threadPoolConfig.getMaximumPoolSize() * noOfCores;
        long keepAliveTimeInMilliSeconds = threadPoolConfig.getKeepAliveTimeInMilliSeconds();

        fixedThreadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
            keepAliveTimeInMilliSeconds, TimeUnit.MILLISECONDS, workQueue, threadFactory);

        fixedThreadPoolExecutor.allowCoreThreadTimeOut(threadPoolConfig.getAllowCoreThreadTimeOut());
        fixedThreadPoolExecutor.prestartAllCoreThreads();
        return fixedThreadPoolExecutor;
    }
}
