package com.pangaea.pubsub.server.controller;

import com.pangaea.pubsub.server.representation.request.SubscribeRequest;
import com.pangaea.pubsub.server.service.SubscriptionService;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@AllArgsConstructor
public class SubscriptionController {

    protected final SubscriptionService subscriptionService;

    @PostMapping("/subscribe/{topic}")
    public void subscribe(@PathVariable String topic, @RequestBody @Valid SubscribeRequest subscribeRequest)
        throws InterruptedException {
        log.info("Received a subscribe request. topic={}, url={}", topic, subscribeRequest.getUrl());
        subscriptionService.subscribeToTopic(topic, subscribeRequest.getUrl());
    }
}
