package com.pangaea.pubsub.server.representation.response;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PublishEventResponse {
    private String status;
}
