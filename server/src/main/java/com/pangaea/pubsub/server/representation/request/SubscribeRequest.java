package com.pangaea.pubsub.server.representation.request;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.URL;

@Getter
@Setter
public class SubscribeRequest {

    @NotNull
    @URL(protocol = "http")
    String url;
}
