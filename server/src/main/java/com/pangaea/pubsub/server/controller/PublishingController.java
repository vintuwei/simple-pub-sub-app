package com.pangaea.pubsub.server.controller;

import com.pangaea.pubsub.server.service.PublishingService;
import java.util.Map;
import javax.validation.constraints.NotEmpty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class PublishingController {

    protected final PublishingService publishingService;

    @PostMapping("/publish/{topic}")
    public void publish(@PathVariable String topic, @RequestBody @NotEmpty Map<String, Object> body) {
        log.info("Received a publish request. topic={}, body={}", topic, body);

        publishingService.publish(topic, body);
    }
}
