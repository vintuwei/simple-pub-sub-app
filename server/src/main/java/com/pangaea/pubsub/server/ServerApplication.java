package com.pangaea.pubsub.server;

import com.pangaea.pubsub.server.configuration.executor.ThreadPoolConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ServerApplication {

    @Bean("publishingConfig")
    @ConfigurationProperties(prefix = "executors.publishing-executor")
    public ThreadPoolConfig migrationsConfig() {
        return new ThreadPoolConfig();
    }

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

}
