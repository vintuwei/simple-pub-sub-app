package com.pangaea.pubsub.server.redis.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("TopicSubscriber")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TopicSubscriber {

    @Id
    private String topic;
    List<String> urls;
}
