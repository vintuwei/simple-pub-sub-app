package com.pangaea.pubsub.server.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pangaea.pubsub.server.api.client.SubscriberApiClient;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
public class RetrofitConfiguration {

    // dummy base subscriber url
    String baseSubscriberUrl = "http://localhost:9000";

    @Bean
    public SubscriberApiClient providesFirebaseRetrofitClient() {
        return getRetrofit(baseSubscriberUrl, null).create(SubscriberApiClient.class);
    }

    private Retrofit getRetrofit(String baseUrl, Interceptor authInterceptor) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        if (authInterceptor != null) {
            httpClientBuilder.addInterceptor(authInterceptor);
        }

        ObjectMapper objectMapper = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return new Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(httpClientBuilder.build())
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .build();
    }
}
