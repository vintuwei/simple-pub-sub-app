package com.pangaea.pubsub.server.redis.repository;

import com.pangaea.pubsub.server.redis.model.TopicSubscriber;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicSubscriberRepository extends CrudRepository<TopicSubscriber, String> {}
