package com.pangaea.pubsub.client.representation.request;

import java.util.Map;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TestRequest {

    @NotNull
    String topic;

    @NotEmpty
    Map<String, Object> data;
}
