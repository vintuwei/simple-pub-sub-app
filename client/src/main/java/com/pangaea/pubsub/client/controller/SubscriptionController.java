package com.pangaea.pubsub.client.controller;

import com.pangaea.pubsub.client.representation.request.TestRequest;
import com.pangaea.pubsub.client.representation.response.TestResponse;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class SubscriptionController {

    @PostMapping("/{path:test1|test2}")
    public ResponseEntity<TestResponse> test(@PathVariable String path, @RequestBody @Valid TestRequest payload) {
        log.info("Received Test Event for path /{}. topic={}, event={}", path, payload.getTopic(), payload.getData());

        TestResponse response = TestResponse.builder()
            .status("OK")
            .build();
        return ResponseEntity.ok(response);
    }
}
