package com.pangaea.pubsub.client.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pangaea.pubsub.client.representation.response.TestResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class SubscriptionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testPostTestEventReturnsSuccessfully() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
            .post("/test1")
            .content("{\n" +
                "    \"topic\": \"test\",\n" +
                "    \"data\": {\n" +
                "        \"name\": \"Vincent\"\n" +
                "    }\n" +
                "}")
            .contentType(MediaType.APPLICATION_JSON))
            .andReturn();

        assertEquals(200, result.getResponse().getStatus());

        String responseStr = result.getResponse().getContentAsString();
        TestResponse testResponse = new ObjectMapper().readValue(responseStr, TestResponse.class);
        assertEquals(testResponse.getStatus(), "OK");
    }

    @Test
    public void testPostTestEventFailsForInvalidURL() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
            .post("/test3")
            .content("{\n" +
                "    \"topic\": \"test\",\n" +
                "    \"data\": {\n" +
                "        \"name\": \"Vincent\"\n" +
                "    }\n" +
                "}")
            .contentType(MediaType.APPLICATION_JSON))
            .andReturn();

        assertEquals(404, result.getResponse().getStatus());
    }
}
