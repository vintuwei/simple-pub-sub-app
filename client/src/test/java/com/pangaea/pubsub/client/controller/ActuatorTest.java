package com.pangaea.pubsub.client.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class ActuatorTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetHealthStatusReturnsSuccessfully() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
            .get("/health")
            .contentType(MediaType.APPLICATION_JSON))
            .andReturn();

        assertEquals(200, result.getResponse().getStatus());

        String responseStr = result.getResponse().getContentAsString();
        Map<String, Object> map = new ObjectMapper().readValue(responseStr, new TypeReference<>() {});

        String status = (String) map.get("status");
        assertEquals(status, "UP");
    }
}
